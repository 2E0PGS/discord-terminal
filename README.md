# Discord-Terminal #

## Terminal client for DiscordApp ##

Discord-Terminal allows you to access DiscordApp and enables you to chat in your favourite channels using a Terminal. This makes DiscordApp behave in a IRC like fashion. I have based a few commands around IRSSI a IRC client for Linux and Mac.  

### How do I get set up? ###

* Clone this repo: `git clone https://2E0PGS@bitbucket.org/2E0PGS/discord-terminal.git`
* Edit `config.example` and add Discord API token. Then save it as `config.json`
* Install NodeJS version 6 or newer.
* Install npm (Node Package Manager).
* Run npm update in the source folder: `npm update`
* Linux: `./start-discord-terminal.sh` Windows: `start-discord-terminal.bat`

### Notice ###

* DiscordApp are not fond of people using custom clients. This means we have to use the BOT API which is confusing for others unless they know.
* I have just started refactoring and working on this project again.

### Licence ###

```
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
