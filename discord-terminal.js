/*
Discord-Terminal is a Terminal Client for DiscordApp written in Node.js

Copyright (C) <2015-2017>  <Peter Stevenson (2E0PGS)>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// --------------Variables--------------
var discord = require("discord.js"); // Core library for Discord.
var multiline = require("multiline"); // Used so we can have nice multiline /help responce.
var colors = require('colors'); // Used for multicolour terminal.
var configFile = require("./config.json"); // Stores Discord API token and user settings.
var fs = require('fs'); // Used so we can update configFile with user options.

var myClient = new discord.Client(); // Create a new instance of the Disord client.

var selectedChannel = configFile.selectedChannel; // Stores current / last selected channel.
var timestampEnabled = configFile.timestampEnabled; // Stores if user wants timestamps displayed.
var lastMessage; // Stores the last recieved message.
var lastMessagePM // Stores the last recieved Private Message message.
// --------------Variables-----------END

myClient.login(configFile.token); // Login to server.

myClient.on("ready", function() {
	console.log("########################################################");
	console.log("Connected to DiscordApp. You are currently connected to: " + myClient.guilds.size + " Discord app servers, and " + myClient.channels.size + " channels.");
	console.log("For a list of commands type /commands");
	console.log("To join a channel do /channels and then do /join [channelname]");
	console.log("########################################################");
	myClient.user.setGame("Discord-Terminal");
});

myClient.on("disconnected", function() {
	var timestamp = new Date();
	console.log((timestamp) + " Lost connection to DiscordApp servers.");
	// Add reconnect script here at some point. Use timeout and a function for login.
});

myClient.on("message", function(msg) { // Anytime we receive a new message check what channel and server.
	if (msg.author.id != myClient.user.id){ // Prevent bot triggering itself.
		if (msg.channel.type === "dm") { // If we recieve a Priave Message.
			if (timestampEnabled === "true") {
				var timestamp = new Date().toLocaleTimeString(); // Time only.
				console.log(colors.bgMagenta((timestamp) + " - " + msg.author.username + ": " + msg));
			}
			else {
				console.log(colors.bgMagenta(msg.author.username + ": " + msg));
			}
			lastMessagePM = msg;
			for (var url in msg.attachments.array()) { // If we get a image process image URL and print URL to terminal.
				console.log(msg.attachments.array()[url].url);
			}
		}
		else if (msg.channel.id === selectedChannel) { // If we recieve a Channel Message.
			if (timestampEnabled === "true") {
				var timestamp = new Date().toLocaleTimeString(); // Time only.
				console.log(colors.bgBlue((timestamp) + " - " + msg.author.username + ": " + msg));
			}
			else{
				console.log(colors.bgBlue(msg.author.username + ": " + msg));
			}
			lastMessage = msg;
			for (var url in msg.attachments.array()) { // If we get a image process image URL and print URL to terminal.
				console.log(msg.attachments.array()[url].url);
			}
		}
		else {
			// Maybe print something if we missed messages because we are not in that channel. Add a counter and tab bar.
		}
	}
});

// ------------Inputs-From-Terminal-Interface------------
process.stdin.setEncoding('utf8');

process.stdin.on('data', function (terminalInputRaw) {

	var terminalInput = terminalInputRaw.replace(/(\r\n|\n|\r|\t)/gm,""); // Trim the \r\n off the end of string.
	var terminalInputPrefix = terminalInput.split(" ", 1).toString(); // Take first word delimited by space.
	var terminalInputSuffix = terminalInput.substring(terminalInputPrefix.length + 1); // Takes anything after the first word.

	if (terminalInput === '/n' || terminalInput === '/users') {
		console.log("Number of Users: " + myClient.users.size);
		var list = myClient.users.array().map(u => colors.bgGreen(u.username + "(" + u.id + "," + u.presence.status + ")"));
		console.log("Available Users: " + list);
	}
	else if (terminalInput === '/commands' || terminalInput === '/help') {
		console.log(commands);
	}
	else if (terminalInput === '/about') {
		console.log(about);
	}
	else if (terminalInput === '/quit') {
		console.log("Quitting Discord-Terminal");
		process.exit();
	}
	else if (terminalInput === '/channels') {
		console.log("Number of Available Channels: " + myClient.channels.size);
		var list = myClient.channels.array().map(c => colors.bgGreen(c.name + "(" + + c.id + "," + c.type + ")"));
		console.log("Available Channels: " + list);
	}
	else if (terminalInput === '/servers') {
		console.log("Number of Available Servers: " + myClient.guilds.size);
		var list = myClient.guilds.array().map(g => colors.bgGreen(g.name + "(" + g.id + ")"));
		console.log("Available Servers: " + list);
	}
	else if (terminalInputPrefix === '/join') {
		selectedChannel = terminalInputSuffix;
		configFile.selectedChannel = terminalInputSuffix;
		fs.writeFileSync("./ConfigFile.json", JSON.stringify(configFile, null, "	"));
		console.log("Joined channel: " + selectedChannel)
	}
	else if (terminalInputPrefix === '/timestamp') {
		timestampEnabled = "true";
		configFile.timestampEnabled = "true";
		fs.writeFileSync("./ConfigFile.json", JSON.stringify(configFile, null, "	"));
		console.log("Enabled Timestamps")
	}
	else if (terminalInputPrefix === '/notimestamp') {
		timestampEnabled = "false";
		configFile.timestampEnabled = "false";
		fs.writeFileSync("./ConfigFile.json", JSON.stringify(configFile, null, "	"));
		console.log("Disabled Timestamps")
	}
	else if (terminalInputPrefix === '/nick') {
		var newNickname = terminalInputSuffix;
		myClient.user.setUsername(newNickname);
		console.log("Changed Nick to: " + newNickname);
	}
	else if (terminalInputPrefix === '/setavatar') {
		var newAvatarLocation = terminalInputSuffix;
		myClient.user.setAvatar(newAvatarLocation);
		console.log("Changed Avatar");
	}
	else if (terminalInputPrefix === '/setgame') {
		var newGameStatus = terminalInputSuffix;
		myClient.user.setGame(newGameStatus);
		console.log("Changed Playing to: " + newGameStatus);
	}
	else if (terminalInputPrefix === '/sendfile') {
		var fileLocation = terminalInputSuffix;
		myClient.channels.get(selectedChannel).sendFile(fileLocation);
		console.log("Attachment Sent");
	}
	else if (terminalInput === '/online') {
		myClient.user.setStatus("online");
	}
	else if (terminalInput === '/idle') {
		myClient.user.setStatus("idle");
	}
	else if (terminalInput === '/invisible') {
		myClient.user.setStatus("invisible");
	}
	else if (terminalInput === '/dnd') {
		myClient.user.setStatus("dnd");
	}
	else if (terminalInputPrefix === '/reply') {
		// Send a reply to last channel message.
		myClient.channels.get(selectedChannel).send(lastMessage.author.toString() + " " + terminalInputSuffix);
	}
	else if (terminalInputPrefix === '/replypm') {
		// Send a reply to last Private Message.
		lastMessagePM.author.send(terminalInputSuffix);
	}
	else if (terminalInputPrefix === '/pm') {
		// Send a Private Message to a user ID.
		//myClient.fetchUser(terminalInputSuffix.split(" ", 1)).then(user => {user.send("TODO")});
	}
	else if (terminalInputPrefix === '/me') {
		// We have to add /me as it would otherwise be blocked by "/" filter as it looks like a command.
		myClient.channels.get(selectedChannel).send("*" + terminalInputSuffix + "*");
	}
	else if (terminalInputPrefix === '/tableflip') {
		// Adding tableflip. This is a desktop client command.
		myClient.channels.get(selectedChannel).send("(╯°□°）╯︵ ┻━┻");
	}
	else if (terminalInputPrefix === '/tts') {
		// Send TTS message.
		myClient.channels.get(selectedChannel).send(terminalInputSuffix, {tts:true});
	}
	else if (terminalInputPrefix === '/clear') {
		// Clears the terminal. Tested on Linux & Windows.
		console.log('\033[2J');
	}
	else if (terminalInput[0] != '/' && terminalInput !== "") { // If we are not using a command or sending a blank message.
		//console.log("treating as message");
		myClient.channels.get(selectedChannel).send(terminalInput);
	}
});
// ------------Inputs-From-Terminal-Interface---------END

var commands = multiline(function() {/*
Help Commands:------------------------------------------
/n                Lists online users in current channel.
/channels         Lists avaliable channels.
/join <channelID> Changes channel.
/timestamp        Enables timestamping of msgs.
/notimestamp      Disables timestamping of msgs.
/nick <nickname>  Changes your username.
/tts <message>    Sends a tts message.
/reply <message>  Mentions last sender.
/replypm          Replies to last PM.
/clear            Clears the terminal window.
/tableflip        Sends tableflip.
/online           Sets status online.
/idle             Sets status idle/away.
/invisible        Sets status invisible.
/dnd              Sets status do not distrub.
/quit             Quits Discord-Terminal.
/setavatar <file> Sets the avatar.
/setgame <game>   Changes playing status.
/sendfile <file>  Sends attachment to channel.
--------------------------------------------------------
*/});

var about = multiline(function() {/*
About:-----------------------------------------------
Discord-Terminal written by: Peter Stevenson (2E0PGS)
Version: 2.0.0
Licence: GNU GPL Version 3
-----------------------------------------------------
*/});
